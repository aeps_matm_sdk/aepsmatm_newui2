package com.matm.matmsdk.aepsmodule.balanceenquiry;

import android.util.Base64;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAEPS2RequestModel;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAPI;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryContract;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryRequestModel;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryResponse;
import com.matm.matmsdk.aepsmodule.cashwithdrawal.AepsResponse;
import com.matm.matmsdk.aepsmodule.ministatement.StatementResponse;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;
import com.matm.matmsdk.aepsmodule.utils.Session;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class BalanceEnquiryPresenter implements com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryContract.View balanceEnquiryContractView;
    private AEPSAPIService aepsapiService;
    private Session session;
    /**
     * Initialize LoginPresenter
     */
    public BalanceEnquiryPresenter(BalanceEnquiryContract.View balanceEnquiryContractView) {
        this.balanceEnquiryContractView = balanceEnquiryContractView;
    }




    @Override
    public void performBalanceEnquiry(String retailer,String token, BalanceEnquiryRequestModel balanceEnquiryRequestModel) {

        balanceEnquiryContractView.showLoader();

        if (this.aepsapiService == null) {
            this.aepsapiService = new AEPSAPIService();
        }

        com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAPI balanceEnquiryAPI =this.aepsapiService.getClient().create(com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAPI.class);
        System.out.println("Request:"+balanceEnquiryRequestModel.toString());

        balanceEnquiryAPI.checkBalanceEnquiry(retailer,token,balanceEnquiryRequestModel).enqueue(new Callback<BalanceEnquiryResponse>() {
            @Override
            public void onResponse(Call<BalanceEnquiryResponse> call, Response<BalanceEnquiryResponse> response) {

                if(response.isSuccessful()) {
                    if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {

                        System.out.println("Response:"+response.body().toString());

                        balanceEnquiryContractView.hideLoader();
                        balanceEnquiryContractView.checkBalanceEnquiryStatus(response.body().getStatus(), response.body().getStatusDesc(),response.body());
                    }else{
                        balanceEnquiryContractView.hideLoader();
                        balanceEnquiryContractView.checkBalanceEnquiryStatus("", "Balance Enquiry Failed",null);
                    }

                }else{
                    balanceEnquiryContractView.hideLoader();
                    balanceEnquiryContractView.checkBalanceEnquiryStatus("", "Balance Enquiry Failed",null);
                }
            }

            @Override
            public void onFailure(Call<BalanceEnquiryResponse> call, Throwable t) {
                balanceEnquiryContractView.hideLoader();
                balanceEnquiryContractView.checkBalanceEnquiryStatus("", "Balance Enquiry Failed",null);
            }
        });
    }
    @Override
    public void performBalanceEnquiryAEPS2(final String token, final BalanceEnquiryAEPS2RequestModel balanceEnquiryRequestModel, String transaction_type) {
        balanceEnquiryContractView.showLoader();
        if (this.aepsapiService == null) {
            this.aepsapiService = new AEPSAPIService();
        }
        if(transaction_type.equalsIgnoreCase("Request Balance")) {

            AndroidNetworking.get("https://vpn.iserveu.tech/generate/v76")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                //encodedUrl = "https://indusindaeps.iserveu.online/aeps2/ibl/balanceEnqury";
                                encryptBalanceEnquiry(token, balanceEnquiryRequestModel, encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }else{

            /**
             *  Mini statement AEPS2
             *
             * */
            AndroidNetworking.get("https://vpn.iserveu.tech/generate/v78")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                // String encoded_url  = "https://indusindaeps.iserveu.online/aeps2/ibl/miniStatement";
                                encryptStatementEnquiry(token,balanceEnquiryRequestModel,encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

            // String encoded_url  = "https://indusindaeps.iserveu.online/aeps2/ibl/miniStatement";
            //encryptStatementEnquiry(token,balanceEnquiryRequestModel,encoded_url);
        }
    }

    public void encryptBalanceEnquiry(final String token, final BalanceEnquiryAEPS2RequestModel balanceEnquiryRequestModel, String encodedUrl){
        com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAPI balanceEnquiryAPI =this.aepsapiService.getClient().create(com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAPI.class);

        balanceEnquiryAPI.checkBalanceEnquiryy(token,balanceEnquiryRequestModel,encodedUrl).enqueue(new Callback<AepsResponse>() {
            @Override
            public void onResponse(Call<AepsResponse> call, Response<AepsResponse> response) {

                try {
                    //if(response.body()!=null){

                        if(response.isSuccessful()) {
                            if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {

                                balanceEnquiryContractView.hideLoader();
                                balanceEnquiryContractView.checkBalanceEnquiryAEPS2(response.body().getStatus(), "Balance Enquiry Success",response.body());
                            }
                            else{
                                balanceEnquiryContractView.hideLoader();
                                balanceEnquiryContractView.checkBalanceEnquiryAEPS2("", "Balance Enquiry Failed",null);
                            }

                        }else {

                            if (response.errorBody() != null) {
                                JsonParser parser = new JsonParser();
                                JsonElement mJson = null;
                                try {
                                    mJson = parser.parse(response.errorBody().string());
                                    Gson gson = new Gson();
                                    AepsResponse errorResponse = gson.fromJson(mJson, AepsResponse.class);
                                    balanceEnquiryContractView.hideLoader();
                                    balanceEnquiryContractView.checkBalanceEnquiryAEPS2(errorResponse.getStatus(), "Balance Enquiry Failed", errorResponse);

                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                    balanceEnquiryContractView.hideLoader();
                                    balanceEnquiryContractView.checkBalanceEnquiryAEPS2("", "Balance Enquiry Failed", null);
                                }
                            } else {
                                balanceEnquiryContractView.hideLoader();
                                balanceEnquiryContractView.checkBalanceEnquiryAEPS2("", "Balance Enquiry Failed", null);

                            }
                        }

                }catch (Exception e){
                    balanceEnquiryContractView.hideLoader();
                    balanceEnquiryContractView.checkBalanceEnquiryAEPS2("", "Balance Enquiry Failed",null);

                }

            }

            @Override
            public void onFailure(Call<AepsResponse> call, Throwable t) {
                balanceEnquiryContractView.hideLoader();
                balanceEnquiryContractView.checkBalanceEnquiryAEPS2("", "Balance Enquiry Failed",null);
            }
        });
    }

    public void encryptStatementEnquiry(final String token, final BalanceEnquiryAEPS2RequestModel balanceEnquiryRequestModel, String encodedUrl){
        com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAPI balanceEnquiryAPI = this.aepsapiService.getClient().create(BalanceEnquiryAPI.class);

        balanceEnquiryAPI.checkStatementEnquiryy(token,balanceEnquiryRequestModel,encodedUrl).enqueue(new Callback<StatementResponse>() {
            @Override
            public void onResponse(Call<StatementResponse> call, Response<StatementResponse> response) {

                try{
                    if(response.body()!=null){
                        if(response.isSuccessful()) {
                            if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {

                                balanceEnquiryContractView.hideLoader();
                                balanceEnquiryContractView.checkStatementEnquiryAEPS2(response.body().getStatus(), "Statement Enquiry Success",response.body());
                            }else{
                                balanceEnquiryContractView.hideLoader();
                                balanceEnquiryContractView.checkStatementEnquiryAEPS2("", "Statement Enquiry Failed",null);
                            }

                        }else{

                            if(response.errorBody() != null) {
                                JsonParser parser = new JsonParser();
                                JsonElement mJson = null;
                                try {
                                    mJson = parser.parse(response.errorBody().string());
                                    Gson gson = new Gson();
                                    StatementResponse errorResponse = gson.fromJson(mJson, StatementResponse.class);
                                    balanceEnquiryContractView.hideLoader();
                                    balanceEnquiryContractView.checkStatementEnquiryAEPS2(errorResponse.getStatus(), "Statement Enquiry Failed",errorResponse);

                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                    balanceEnquiryContractView.hideLoader();
                                    balanceEnquiryContractView.checkStatementEnquiryAEPS2("", "Statement Enquiry Failed",null);
                                }
                            }else{
                                balanceEnquiryContractView.hideLoader();
                                balanceEnquiryContractView.checkStatementEnquiryAEPS2("", "Statement Enquiry Failed",null);

                            }
                        }

                    }
                }catch (Exception exc){
                   // exc.printStackTrace();
                    balanceEnquiryContractView.hideLoader();
                    balanceEnquiryContractView.checkStatementEnquiryAEPS2("", "Statement Enquiry Failed",null);

                }
            }

            @Override
            public void onFailure(Call<StatementResponse> call, Throwable t) {
                balanceEnquiryContractView.hideLoader();
                balanceEnquiryContractView.checkStatementEnquiryAEPS2("", "Statement Enquiry Failed",null);
            }
        });
    }

}
